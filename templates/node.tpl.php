<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> node-body clearfix" <?php print $attributes; ?>>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <?php if (neverland_meta_has_items($node, $content)): ?>
                <?php if (neverland_show_date($node)): ?>
                    <td class="node-meta">
                        <div class="node-date-wrapper">
                            <div class="node-date">
                                <div class="node-dt-small">
                                    <?php print strtoupper(date('M', $node->created)); ?>
                                </div>

                                <div class="node-dt-big">
                                    <?php print date('j', $node->created); ?>
                                </div>

                                <div class="node-dt-small">
                                    <?php print date('Y', $node->created); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                <?php else: ?>
                    <td class="node-sep"></td>
                <?php endif; ?>
            <?php endif; ?>
            
            <td rowspan="2" class="node-content
                <?php if (!neverland_meta_has_items($node, $content)): ?>
                    node-nopad
                <?php endif; ?>">

                <?php print render($title_prefix); ?>

                <h2 <?php print $title_attributes; ?>>
                    <a href="<?php print $node_url; ?>">
                        <?php print $title; ?>
                    </a>
                </h2>

                <?php print render($title_suffix); ?>

                <?php if ($display_submitted && $node->uid): ?>
                    <div class="meta submitted">
                        <i class="icon-user"></i>
                        <?php print t('Submitted by') . neverland_profile_url('name', $node->name); ?>
                    </div>
                <?php endif; ?>

                <div class="content"<?php print $content_attributes; ?>>
                    <?php
                        // We hide the comments and links now so that we can render them later.
                        hide($content['comments']);
                        hide($content['links']);
                        hide($content['taxonomy_vocabulary_1']);
                        hide($content['taxonomy_vocabulary_2']);
                        print render($content);
                    ?>
                </div>

                <?php
                    // Remove the "Add new comment" link on the teaser page or if the comment
                    // form is being displayed on the same page.
                    if ($teaser || !empty($content['comments']['comment_form'])) {
                        unset($content['links']['comment']['#links']['comment-add']);
                    }

                    // Only display the wrapper div if there are links.
                    $links = render($content['links']);
                    if ($links):
                ?>
                    <div class="link-wrapper">
                        <?php print $links; ?>
                    </div>
                <?php endif; ?>

                <?php print render($content['comments']); ?>
            </td>
        </tr>

        <?php if (neverland_meta_has_items($node, $content)): ?>
            <tr>
                <td class="node-sep">
                    <?php
                        print neverland_taxonomy_image($content['taxonomy_vocabulary_1']);
                        print neverland_profile_url('avatar', $node->name);
                    ?>
                </td>
            </tr>
        <?php endif; ?>
    </table>
</div>