<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>>
    <head profile="<?php print $grddl_profile; ?>">
        <?php print $head; ?>

        <title>
            <?php print $head_title; ?>
        </title>

        <?php print $styles; ?>
        <link href="//cdn.kde.org/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen, projection" />
        <link href="//cdn.kde.org/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" media="screen, projection" />
        <link href="//cdn.kde.org/css/bootstrap-drupal.css" rel="stylesheet" type="text/css" media="screen, projection" />
    </head>

    <body class="<?php print $classes; ?>" <?php print $attributes;?>>
        <script type="text/javascript" src="//cdn.kde.org/js/jquery.js"></script>
        <script type="text/javascript" src="//cdn.kde.org/js/bootstrap.js"></script>
        <script type="text/javascript" src="//cdn.kde.org/js/bootstrap-neverland.js"></script>

        <div id="skip-link">
            <a href="#main-content" class="element-invisible element-focusable">
                <?php print t('Skip to main content'); ?>
            </a>
        </div>

        <?php print $page_top; ?>
        <?php print $page; ?>
        <?php print $scripts; ?>
        <?php print $page_bottom; ?>
    </body>
</html>
