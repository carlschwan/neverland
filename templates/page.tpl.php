<?php print render($page['header']); ?>

<div id="header" class="navbar navbar-static-top Neverland">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target="#neverland-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <?php if ($logo): ?>
                <a class="brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                </a>
            <?php endif; ?>

            <div id="neverland-menu" class="nav-collapse">
                <ul class="nav">
                    <?php print neverland_generate_menu('header'); ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?php if ($main_menu): ?>
        <div class="row">
            <div class="span12">
                <div class="subnav main-menu">
                    <div class="pull-right">
                        <?php print $feed_icons; ?>
                    </div>

                    <ul class="nav nav-pills">
                        <?php print neverland_generate_menu('main-menu'); ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($messages): ?>
        <div class="row">
            <div id="messages" class="span12">
                <?php print $messages; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($page['featured']): ?>
        <div class="row">
            <div id="featured" class="span12">
                <?php print render($page['featured']); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row" id="content">
        <div class="span9">
            <?php print render($title_prefix); ?>

            <?php print render($title_suffix); ?>

            <?php if ($tabs): ?>
                <?php print render($tabs); ?>
            <?php endif; ?>

            <?php print render($page['help']); ?>

            <?php if ($action_links): ?>
                <div class="well well-small">
                    <ul class="nav nav-pills no-margin">
                        <?php print render($action_links); ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if (!drupal_is_front_page() || !theme_get_setting('neverland_hide_home')): ?>
                <?php print render($page['content']); ?>
            <?php endif; ?>
        </div>
        <div class="span3" id="sidebar">
            <?php if ($page['sidebar_first']): ?>

                <?php print render($page['sidebar_first']); ?>

            <?php endif; ?>

        </div>
    </div>

    <?php if ($page['footer']): ?>
        <footer class="copyright align-center">
            <?php print render($page['footer']); ?>
        </footer>
    <?php endif; ?>
</div>
