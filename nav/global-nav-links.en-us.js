/**
* KDE-www Global Navigation
*
* @category Navigation links
* @language English (United States)
* @copyright (c) 2012 KDE Webteam
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

$nav.sites.push(['Homepage',         'http://kde.org']);
$nav.sites.push(['News',             'http://dot.kde.org']);
$nav.sites.push(['Community Forums', 'http://forum.kde.org']);
$nav.sites.push(['UserBase Wiki',    'http://userbase.kde.org']);
$nav.sites.push(['TechBase Wiki',    'http://techbase.kde.org']);
$nav.sites.push(['Community Wiki',   'http://community.kde.org']);
$nav.sites.push(['Documentation',    'http://docs.kde.org']);
$nav.sites.push(['Planet',           'http://planet.kde.org']);
$nav.sites.push(['Bugzilla',         'http://bugs.kde.org']);
$nav.sites.push(['Developer Blogs',  'http://blogs.kde.org']);