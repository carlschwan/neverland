<?php

/**
* Generates the Neverland theme setting form
*/
function neverland_form_system_theme_settings_alter(&$form, &$form_state) {
    $form['theme_settings']['neverland_taxonomy_image'] = array(
        '#type' => 'checkbox',
        '#title' => t('Taxonomy image'),
        '#default_value' => theme_get_setting('neverland_taxonomy_image'),
    );

    $form['theme_settings']['neverland_node_avatar'] = array(
        '#type' => 'checkbox',
        '#title' => t('Node author avatar'),
        '#default_value' => theme_get_setting('neverland_node_avatar'),
    );

    $form['theme_settings']['neverland_date_hide_nouid'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide node date for empty author'),
        '#default_value' => theme_get_setting('neverland_date_hide_nouid'),
    );

    $form['theme_settings']['neverland_date_hide_always'] = array(
        '#type' => 'checkbox',
        '#title' => t('Always hide node date'),
        '#default_value' => theme_get_setting('neverland_date_hide_always'),
    );

    $form['theme_settings']['neverland_hide_home'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide the home page content'),
        '#default_value' => theme_get_setting('neverland_hide_home'),
    );

    $form['theme_settings']['neverland_category_menus'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable mysite.com/category specific menus'),
        '#default_value' => theme_get_setting('neverland_category_menus'),
    );
}

?>
