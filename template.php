<?php

/**
 * Override or insert variables into the page template.
 */
function neverland_process_page(&$variables) {
    // Hook into color.module.
    if (module_exists('color')) {
        _color_page_alter($variables);
    }

    // Always print the site name and slogan, but if they are toggled off, we'll
    // just hide them visually.
    $variables['hide_site_name']   = !theme_get_setting('toggle_name');
    $variables['hide_site_slogan'] = !theme_get_setting('toggle_slogan');

    // If toggle_name is false, the site_name will be empty, so we rebuild it.
    if ($variables['hide_site_name']) {
        $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
    }

    // If toggle_site_slogan is false, the site_slogan will be empty, so we rebuild it.
    if ($variables['hide_site_slogan']) {
        $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
    }

    // Since the title and the shortcut link are both block level elements,
    // positioning them next to each other is much simpler with a wrapper div.
    if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
        // Add a wrapper div using the title_prefix and title_suffix render elements.
        $variables['title_prefix']['shortcut_wrapper'] = array(
            '#markup' => '<div class="shortcut-wrapper clearfix">',
            '#weight' => 100,
        );

        $variables['title_suffix']['shortcut_wrapper'] = array(
            '#markup' => '</div>',
            '#weight' => -99,
        );

        // Make sure the shortcut link is the first item in title_suffix.
        $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
    }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function neverland_preprocess_maintenance_page(&$variables) {
    // By default, site_name is set to Drupal if no db connection is available
    // or during site installation. Setting site_name to an empty string makes
    // the site and update pages look cleaner.
    // @see template_preprocess_maintenance_page
    if (!$variables['db_is_active']) {
        $variables['site_name'] = '';
    }
}

/**
 * Override or insert variables into the maintenance page template.
 */
function neverland_process_maintenance_page(&$variables) {
    // Always print the site name and slogan, but if they are toggled off, we'll
    // just hide them visually.
    $variables['hide_site_name']   = !theme_get_setting('toggle_name');
    $variables['hide_site_slogan'] = !theme_get_setting('toggle_slogan');

    // If toggle_name is false, the site_name will be empty, so we rebuild it.
    if ($variables['hide_site_name']) {
        $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
    }

    // If toggle_site_slogan is false, the site_slogan will be empty, so we rebuild it.
    if ($variables['hide_site_slogan']) {
        $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
    }
}

/**
 * Override or insert variables into the node template.
 */
function neverland_preprocess_node(&$variables) {
    if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
        $variables['classes_array'][] = 'node-full';
    }
}

/**
 * Override or insert variables into the block template.
 */
function neverland_preprocess_block(&$variables) {
    // In the header region visually hide block titles.
    if ($variables['block']->region == 'header') {
        $variables['title_attributes_array']['class'][] = 'hidden';
    }
}

/**
 * Implements theme_menu_tree().
 */
function neverland_menu_tree($variables) {
    return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function neverland_field__taxonomy_term_reference($variables) {
    $output = '';

    // Render the label, if it's not hidden.
    if (!$variables['label_hidden']) {
        $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
    }

    // Render the items.
    $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';

    foreach ($variables['items'] as $delta => $item) {
        $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' .
                       drupal_render($item) .
                   '</li>';
    }

    $output .= '</ul>';

    // Render the top-level DIV.
    $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' .
                  $output .
              '</div>';

    return $output;
}

/**
 * Generates the link menus
 */
function neverland_generate_menu($menu_type) {
    if ($menu_type == 'header') {
        $output = '';
        $count = 0;

        // Fetch the navigation links for the menu
        $links = menu_navigation_links('menu-neverland-header');

        // Build menu items
        foreach ($links as $link) {
            if (!empty($link['attributes']['title'])) {
                $output .= '<li><a href="' . url($link['href']) . '">';
                $output .= '<h2>' . $link['title'];
                $output .= '<small>' . $link['attributes']['title'] . '</small>';
                $output .= '</h2>';
                $output .= '</a></li>';
            }

            // We allow max. 3 links in header
            if (++$count == 3) {
                break;
            }
        }

        return $output;
    }
    else if ($menu_type == 'main-menu') {
        // We fetch the entire menu tree as we want submenus as well
        $menu = neverland_get_main_menu_name();
        $tree = menu_tree_all_data($menu);

        // We hand over the data to the tree parser. This will call itself
        // recirsively for each child item
        return neverland_generate_menu_tree($tree);
    }
}

/**
 * Resolves a menu name based on category settings
 */
function neverland_get_main_menu_name() {
    $menu = 'main-menu';

    if (theme_get_setting('neverland_category_menus')) {
        $base = rtrim(base_path(), '/');
        $uri = str_replace($base, '', request_uri());
        $cat = explode('/', $uri);

        if (isset($cat[1]) && !empty($cat[1])) {
            $menu = "menu-category-{$cat[1]}";
        }
    }

    return $menu;
}

/**
 * Generates menu items from the menu tree
 *
 * This function recursively calls itself if there are more child items
 */
function neverland_generate_menu_tree($tree, $first_child = true) {
    $output = '';

    // Traverse through the menu tree and resolve the structure
    foreach ($tree as $item) {
        $link = $item['link'];

        if (!empty($link['options']['attributes']['title'])) {
            $icon = '<i class="' . $link['options']['attributes']['title'] . '"></i>&nbsp;';
        }
        else {
            $icon = '<i class="icon-globe"></i>&nbsp;';
        }

        if (count($item['below']) == 0) {
            $output .= '<li>';
            $output .= '<a href="' . url($link['link_path']) . '">' . $icon . $link['link_title'] . '</a>';
            $output .= '</li>';
        }
        else {
            if ($first_child) {
                $output .= '<li class="dropdown">';
                $output .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                $output .= $icon . $link['link_title'];
                $output .= '&nbsp;<b class="caret"></b>';
                $output .= '</a>';
                $output .= '<ul class="dropdown-menu">';
                $output .= neverland_generate_menu_tree($item['below'], false);
                $output .= '</ul>';
                $output .= '</li>';
            }
            else {
                $output .= '<li class="dropdown-submenu">';
                $output .= '<a href="#" tabindex="-1">';
                $output .= $icon . $link['link_title'];
                $output .= '</a>';
                $output .= '<ul class="dropdown-menu">';
                $output .= neverland_generate_menu_tree($item['below'], false);
                $output .= '</ul>';
                $output .= '</li>';
            }
        }
    }

    return $output;
}

/**
* Returns an image based on the post's taxonomy term
*/
function neverland_taxonomy_image($taxonomy) {
    global $theme_path;

    // We display taxonomy image only if the user has set the flag
    if (theme_get_setting('neverland_taxonomy_image') && isset($taxonomy[0])) {
        $title = $taxonomy[0]['#title'];
        $href = base_path() . drupal_lookup_path('alias', $taxonomy[0]['#href']);

        // Determine the image file name
        $img = trim($title);
        $img = preg_replace('/\s+/', '-',$img);
        $img = preg_replace('/--+/', '-',$img);
        $img = preg_replace('([^a-zA-Z0-9-])', '', $img);
        $img = strtolower($img);

        // Check if image exists, if not, default to kde.png
        if (!file_exists(realpath("{$theme_path}/taxonomy/{$img}.png"))) {
            $img = 'kde';
        }

        // Generate the image link
        $url = url($href);
        $img = theme('image', array(
            'path'   => "{$theme_path}/taxonomy/{$img}.png",
            'title'  => $title,
        ));

        return '<a href="' . $href . '" class="thumbnail">' . $img . '</a>';
    }
    else {
        return null;
    }
}

/**
* Returns user profile link for a user
*/
function neverland_profile_url($output, $name) {
    // Get the user's profile info
    $name = strip_tags($name);
    $fields = user_load_by_name($name);

    if (isset($fields->uid)) {
        $profile = null;

        // Generate the user's profile URL
        $alias = drupal_lookup_path('alias', "user/{$fields->uid}");

        if (!empty($alias)) {
            $href = base_path() . $alias;
        }
        else {
            $href = base_path() . "user/{$fields->uid}";
        }

        // Resolve output based on request
        if ($output == 'name') {
            $name = neverland_resolve_name($fields);
            $profile = ' <a href="' . $href . '">' . $name . '</a> ';
        }
        else if ($output == 'avatar') {
            $avatar = neverland_resolve_avatar($fields);

            if ($avatar != null) {
                $profile = '<a href="' . $href . '" class="thumbnail">' . $avatar . '</a>';
            }
        }

        return $profile;
    }
    else {
        return " {$name} ";
    }
}

/**
* Resolves real name of a user from profile fields data
*/
function neverland_resolve_name($fields) {
    // These are the available full name keys
    $keys = array('profile_name', 'realname');

    if ($fields != null) {
        // We traverse through all available full name keys
        // If data is found against a key, we return it
        foreach($keys as $key) {
            if (!empty($fields->$key)) {
                return $fields->$key;
            }
        }
    }

    // No name found, return the username
    return $fields->name;
}

/**
* Resolves user avatar image
*/
function neverland_resolve_avatar($fields) {
    $avatar = null;

    // We display the avatar only if user has set the flag
    if (theme_get_setting('neverland_node_avatar')) {
        // Check if the user has a profile picture set
        // If not, fall back to a default avatar
        if (isset($fields->picture) && !empty($fields->picture->uri)) {
            $avatar = '<img src="' . file_create_url($fields->picture->uri) . '" alt="" />';
        }
        else if (file_exists(drupal_realpath('public://default-avatar.png'))) {
            $avatar = '<img src="' . file_create_url('public://default-avatar.png') . '" alt="" />';
        }
    }

    return $avatar;
}

/**
* Checks if date has to be shown for the node
*/
function neverland_show_date($node) {
    return !(theme_get_setting('neverland_date_hide_always') ||
            (theme_get_setting('neverland_date_hide_nouid') && !$node->uid));
}

/**
* Get the width of the meta information column
*/
function neverland_meta_has_items($node, $content) {
    $show_date = neverland_show_date($node);
    $show_taxonomy = theme_get_setting('neverland_taxonomy_image') && isset($content['taxonomy_vocabulary_1'][0]);
    $show_avatar = theme_get_setting('neverland_node_avatar');

    return $show_date || $show_taxonomy || $show_avatar;
}
