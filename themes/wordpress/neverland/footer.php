<!-- Footer.php:Footer:Start -->
<div id="footerRow">
	<div class="navbar navbar-bottom Neverland">
		<div class="navbar-inner">
			<div class="container">
				<ul class="nav pull-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-list"></i> KDE Links <b class="caret-up"></b>
						</a>
						<ul id="global-nav" class="dropdown-menu bottom-up"></ul>
					</li>
				</ul>
				<ul class="nav">
					<li>
						<a href="<?php echo home_url('/')?>"><i class="icon-home"></i> Home</a>
					</li>
					<li class="divider-vertical"></li>
				</ul>
			</div>
		</div>
	</div>
	<footer class="Neverland">
		<?php wp_meta(); ?>
		<?php wp_footer(); ?>
		<small>
			<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
				<div class="widget-area row">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
			<?php endif; ?>
		</small>
		<p>
			<?php bloginfo('name'); ?> is proudly powered by <a href="http://wordpress.org/">WordPress</a>
			<!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
		</p>
	</footer>
</div>
<!-- Javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-neverland.js"></script>
<script src="http://cdn.kde.org/nav/global-nav.js"></script>
<script type="text/javascript">
	$('.carousel').carousel();
	$('.search-query').blur(function() {
		this.value = "Search..";
	});
</script>
<!--
WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
BE BLOCKED INSTANTLY AND PERMANENTLY!
<?php
	$trapmail = "aaaatrap-";
	$t = pack('N', time());
	for($i=0;$i<=3;$i++) {
   		$trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
	}
	$ip=$_SERVER["REMOTE_ADDR"];
	sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
	$trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);
	echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
?>
WARNING END
-->
</body>
</html>

