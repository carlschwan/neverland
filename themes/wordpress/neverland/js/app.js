/* Check if jQuery is loaded */
if (!jQuery) {
	console.log('Error: jQuery not loaded');
	throw ('jQueryNotLoadedException'); /* Throw to stop execution */
} else {
	$ = jQuery;
}

/* Verify that the body tag is loaded */
if ($('body').length == 0) {
	console.log('Error: Body tag not found. Place the JS inside the body tag to fix this.');
	throw ('BodyTagNotFound'); /* Throw to stop execution */
}

/* Triggers */
$(window).bind('load', initHost);

/* Timer */
setInterval(actionHost, 500);

/* Initialization handler */
function initHost() {
	try {
		doSetBaseImageSizes();
		doFocusThumbnail();
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}

/* Action host */
function actionHost() {
	try {
		doAdjustFloatingWindow();
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}

/* Set the size for the hidden base images */
function doSetBaseImageSizes() {
	try {
		var maxHeight = $('.showflor .screenShot').outerHeight();
		$('.showItem .base').css('maxHeight', maxHeight);
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}

/* Bring up a thumbnail from shelf to the stage */
function doFocusThumbnail() {
	try {
		$('.showItem').click(function() {
			var thumbnail = $(this).children('.screenShot');
			var base = $(this).children('.base');
			var overlay = $(this).children('.overlay');

			var wrapperTop = $('.wrapper').offset().top;
			var wrapperLeft = $('.wrapper').offset().left;

			var thumbnailHeight = thumbnail.outerHeight();
			var thumbnailWidth = thumbnail.outerWidth();
			var thumbnailTop = thumbnail.offset().top - wrapperTop;
			var thumbnailLeft = thumbnail.offset().left - wrapperLeft;

			var baseSrc = base.attr('src');
			var baseWidth = base.outerWidth();
			var baseHeight = base.outerHeight();
			var baseLeft = ($('.showflor').outerWidth() / 2) - (baseWidth / 2);
			var baseTop = $('.showflor').offset().top;

			var floatImageStyle = 'height:' + thumbnailHeight + 'px;';
				floatImageStyle += 'position: absolute;';
				floatImageStyle += 'top:' + thumbnailTop + 'px;';
				floatImageStyle += 'left:' + thumbnailLeft + 'px;';
				floatImageStyle += 'z-index: 50;';
				floatImageStyle += 'opacity: 0;';

			var floatImage = getImageTag({
				id: 'floatImage',
				src: baseSrc,
				style: floatImageStyle
			});

			/* Check if already highlighted */
			if ($(this).hasClass('view')) {
				return;
			}

			$('.showflor .overlay').hide();//Hide the overlay first

			/* Fade out the logo overlay */
			$('#adshelf .appLogo').css('opacity','0.5');

			/* Add the image */
			$('#floatImage').remove();
 			$('.exhibit .screenShot').animate({opacity: 0});
			$('.exhibit').append(floatImage);

			/* Magic lamp! */
			$('#floatImage').animate({
				left: baseLeft.toString() + 'px',
				top: '20px',
				opacity: 1,
				height: baseHeight + 'px'
			},"normal",
				function (){
					$('.showflor .overlay') //Assume position and size of the image as soon as the nimation is over
					.css("top",$("#floatImage").position().top)
					.css("left",$("#floatImage").position().left)
					.css("width",$("#floatImage").width())
					.css("height",$("#floatImage").height())
					.show(); //Show the overlay
				}
			);

			/* Set the overlay */
			$('.showflor .overlay').html(overlay.html());

			/* Set active thumb */
			$('.showItem').removeClass('view');
			$(this).addClass('view');
		});
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}

/* Adjust the floating window position */
function doAdjustFloatingWindow() {
	try {
		var float = $('#floatImage');

		if (float.length != 0 && !float.is(':animated')) {
			var floatWidth = float.outerWidth();
			var floatLeft = ($('.showflor').outerWidth() / 2) - (floatWidth / 2);

			float.css('left', floatLeft);
		}
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}

/* Generate an image tag */
function getImageTag(params) {
	try {
		var html = '<img';

		for (var key in params) {
			html += (' ' + key + '="' + params[key] + '"');
		}

		html += '/>';

		/* return the generated HTML */
		return html;
	} catch (e) {
		console.log('Error: ' + e.message);
	}
}